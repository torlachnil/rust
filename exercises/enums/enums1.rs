// enums1.rs
// Make me compile! Execute `rustlings hint enums1` for hints!

#[derive(Debug)]
enum Message {
    Quit(i64),
    Echo(String),
    Move(u32, u32),
    ChangeColor(u8,u8,u8),
}


fn main() {
    let input = Message::ChangeColor(1,2,3);
    match input {
        Message::Quit(i) => println!("{:?}", i),
        Message::Echo(i) => println!("{:?}", i),
        Message::Move(i, j) => println!("{:?}", i),
        Message::ChangeColor(i, j, k) => println!("{:?}", i),
    }
}
